import { createStore } from 'redux';
import rootReducer from "../reducers/rootReducer";

let initialState = {
    newsletter: {
        pageData: {}
    }
}

let store = createStore(rootReducer, initialState);

export default store;
