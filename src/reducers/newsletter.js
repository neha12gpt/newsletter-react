
import { LOAD_PAGE_DATA } from '../constants'

export function newsletterReducer(state = {}, action) {
    switch (action.type) {
        case LOAD_PAGE_DATA: {
            let { pageData } = action
            let newState = Object.assign({}, state, { pageData: pageData });
            return newState;
        }
        default:
            return state;
    }
}