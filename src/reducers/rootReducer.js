import { combineReducers } from 'redux';
import { newsletterReducer } from './newsletter';

let rootReducer = combineReducers({
    newsletter: newsletterReducer
});

export default rootReducer;

