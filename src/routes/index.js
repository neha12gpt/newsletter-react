import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import NewsLetterContainer from '../components/NewsLetterContainer';

export function Routes() {
    return (
        <Router>
            <div>
                <Switch>
                    <Route component={NewsLetterContainer} />
                </Switch>
            </div>
        </Router>
    )
}