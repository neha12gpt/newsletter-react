import React, { Component } from 'react';

class ThankYou extends Component {
    render() {
        let { location } = this.props;
        let data = location.state;
        return (
            <div className="eut_cmly_kia_newsletter_req">
                <div className="list_wrap">
                    <div className="thx_wrap">
                        <p>We have sent you an email to confirm your subscription.</p>
                    </div>
                    <div className="thx_wrap">
                        <ul>
                            <li className="li65p"><div className="bg_image"></div></li>
                            <li className="li45p">
                                <table className="box">
                                    <tbody>
                                        <tr>
                                            <td>Last name:</td>
                                            <td>{data.lastName}</td>
                                        </tr>
                                        <tr>
                                            <td>First name:</td>
                                            <td>{data.firstName}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>{data.email}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default ThankYou;