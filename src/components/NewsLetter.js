import React, { Component } from 'react';
import classNames from 'classnames';
import axios from 'axios';
import API_NEWSLETTER_POST from '../constants';
import ReactHtmlParser from 'react-html-parser';

class NewsLetter extends Component {
  constructor() {
    super();
    this.state = {
      isFormValid: false,
      firstName: "",
      lastName: "",
      email: "",
      signup: false,
      isValid: { firstName: false, lastName: false, email: false },
      isPristine: { firstName: true, lastName: true, email: true }
    }
  }

  validateField(name, value, isRequired) {
    let { isValid } = this.state;
    switch (name) {
      case 'email':
        const emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        isValid[name] = emailValid;
        break;
      case 'firstName':
        isValid[name] = isRequired ? isRequired && value : true;
        break;
      case 'lastName':
        isValid[name] = isRequired ? isRequired && value : true;
        break;
      default:
        break;
    }
    return isValid;
  }
  validateForm(isValid) {
    const isFormValid = Object.keys(isValid).every(function (k) { return isValid[k] });
    return isFormValid;
  }
  handleChange(e) {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    const isRequired = target.required;
    const { isPristine } = this.state;
    isPristine[name] = false;
    let isValid = this.validateField(name, value, isRequired);
    const isFormValid = this.validateForm(isValid);

    this.setState({
      [name]: value,
      isValid,
      isFormValid,
      isPristine: isPristine
    });

  }
  handleSubmit(e, history) {
    let { firstName, lastName, email } = this.state;
    let data = { firstName, lastName, email, locale: 'hu-hu', csrfToken: '1854270777694141' };

    // axios.post('API_NEWSLETTER_POST', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
    //     .then((res, err) => {
    //         if (res.code === 200 && res.result === 'success') {
    //             history.push('/newsletter/thankyou', { firstName, lastName, email });
    //         } else {
    //             console.log(err);
    //         }
    //     })

    history.push('/thankyou', { firstName, lastName, email });
  }
  toggleSignup(e) {
    let { signup } = this.state;
    this.setState({
      signup: !signup
    });
  }
  renderStep1(pageData) {
    let { step1 } = pageData;
    let { firstName, lastName, email, isValid, isFormValid, isPristine } = this.state;
    return (
      <div className={classNames("inputs_wrap", { active: !isFormValid })}>
        <div className="head">
          <p>1. Personal information</p>
        </div>
        <div className="body">
          <ul className="input_group">
            <li className="input50p">
              <div className="field-table">
                {step1.lastName.isDisplayed &&
                  <div className="field-td">
                    <p className={classNames("error_message", { hidden: isPristine.lastName || isValid.lastName })}>This is not a valid input</p>
                    <span className={classNames("eut_form_plugin", { required: step1.lastName.isRequired })}>
                      <label htmlFor="text3" className="eut_text hidden">
                        Last name
                                            </label>
                      <input placeholder="Last Name" className={classNames("eut_data_right", { "eut_data_wrong": !isValid.lastName && !isPristine.lastName })} type="text" id="text3" name="lastName" required={step1.lastName.isRequired} value={lastName} onChange={e => this.handleChange(e)} />
                    </span>
                  </div>
                }
                {step1.firstName.isDisplayed &&
                  <div className="field-td">
                    <p className={classNames("error_message", { hidden: isPristine.firstName || isValid.firstName })}>This is not a valid input</p>
                    <span className={classNames("eut_form_plugin", { required: step1.firstName.isRequired })}>
                      <label htmlFor="text2" className="eut_text hidden">
                        First name
                                            </label>
                      <input placeholder="First Name" className={classNames("eut_data_right", { "eut_data_wrong": !isValid.firstName && !isPristine.firstName })} name="firstName" type="text" id="text2" required={step1.firstName.isRequired} value={firstName} onChange={e => this.handleChange(e)} />
                    </span>
                  </div>
                }
              </div>
            </li>
            {step1.email.isDisplayed &&
              <li>
                <p className={classNames("error_message", { hidden: isPristine.email || isValid.email })}>This is not a valid input</p>
                <span className={classNames("eut_form_plugin", { required: step1.email.isRequired })}>
                  <label htmlFor="text4" className="eut_text hidden">
                    Email
                                    </label>
                  <input placeholder="Email" className={classNames("eut_data_right", { "eut_data_wrong": !isValid.email && !isPristine.email })} name="email" type="text" id="text4" required={step1.email.isRequired} value={email} onChange={e => this.handleChange(e)} />
                </span>
              </li>
            }
          </ul>
        </div>
      </div>
    )
  }
  renderStep2(pageData) {
    let { isFormValid, signup } = this.state;
    return (
      <div className={classNames("inputs_wrap", { eut_blind: !isFormValid }, { active: isFormValid })}>
        <div className="head">
          <p>2. Additional data</p>
        </div>
        <div className="body">
          <div className="check_submit no-border">
            <p className="consent_text">
              {ReactHtmlParser(pageData.preConsentText)}
            </p>
            <div>
              <span className="eut_form_plugin">
                <p className="error_message hidden"></p>
                <input name="signup" value={signup} type="checkbox" id="check_option_0" />
                <label onClick={e => this.toggleSignup()} className={classNames("eut_checkbox", { active: signup })} htmlFor="check_option_0">
                  Sign up
                                </label>
              </span>
            </div>
          </div>
        </div>
      </div>
    )
  }
  render() {
    let { newsletter, history } = this.props;
    let { isFormValid } = this.state;
    return (
      <div>
        <div className="list_wrap">
          <ul>
            <li>
              {newsletter.pageData.step1 && this.renderStep1(newsletter.pageData)}
            </li>
            <li>
              {newsletter.pageData.step2 && this.renderStep2(newsletter.pageData)}
            </li>
          </ul>
          <div className="notice">
            {newsletter.pageData.disclaimer}
          </div>
          <div className="radiobutton_style">
            <button type="button" onClick={e => this.handleSubmit(e, history)} className="eut_cmpe_btn small" disabled={isFormValid ? false : 'disabled'} >
              Signup
                        </button>
          </div>
        </div>
      </div>
    );
  }
}

export default NewsLetter;
