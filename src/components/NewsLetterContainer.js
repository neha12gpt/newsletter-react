import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";
import { loadPageData } from '../actions/newsletter'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import NewsLetter from '../components/NewsLetter';
import ThankYou from '../components/ThankYou';

class NewsLetterContainer extends Component {

    componentDidMount() {
        let { actions } = this.props;
        actions.loadPageData();
    }
    render() {
        return (
            <div>
                <div className="eut_sub_title">
                    <h1>NewsLetter</h1>
                </div>
                <div className="parsys contents_1">
                    <div className="newsletter section">
                        <div className="eut_cmly_section">
                            <div className="inner">
                                <div className="eut_cmly_kia_newsletter_req">
                                    <Switch>
                                        <Route exact={true} path={`${this.props.match.path}thankyou`} component={ThankYou} />
                                        <Route render={(props) => <NewsLetter {...this.props} />} />
                                    </Switch>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    newsletter: state.newsletter
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({ loadPageData }, dispatch)
})

// `connect` returns a new function that accepts the component to wrap:
const connectToStore = connect(
    mapStateToProps,
    mapDispatchToProps
)

export default connectToStore(NewsLetterContainer);