import { LOAD_PAGE_DATA } from '../constants'

export function loadPageData() {
    return { type: LOAD_PAGE_DATA, pageData: window.eut.newsletterData }
}
