import React, { Component } from 'react';
import './App.css';
import { Routes } from './routes';


class App extends Component {
  render() {
    return (
      <div>
        <hr />
        <div className="body">
          <Routes />
        </div>
      </div>
    );
  }
}

export default App;
